﻿using System;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.Net;
using System.Numerics;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Text.RegularExpressions;
using System.Text;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Data;
using System.Net.NetworkInformation;

namespace CarDealership
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*            Create a multidimensional array to represent the inventory, where each inner
            array represents a vehicle. The fields within the inner array should follow this
            order: id, make of the model, color, odometer reading, and price. Consider
            creating an array of properties so that the index of the property corresponds to
            its position in the inner array.*/

            string[,]? inventory;
            ReadFile(out inventory);
            MainMenu(inventory);

        }
        static void MainMenu(string[,] inventory)
        {
            Console.WriteLine("Welcome to Lucky Auto Sales! ");
            Console.WriteLine("What would you like to do? ");
            Console.WriteLine("1 - Find vehicles that match make/ model");
            Console.WriteLine("2 - Find vehicles that fall within a price range");
            Console.WriteLine("3 - Find vehicles that match a color");
            Console.WriteLine("4 - Show all vehicles");
            Console.WriteLine("5 - Add a vehicle");
            Console.WriteLine("6 - Quit");
            Console.WriteLine("Enter your command:");
            string command = Console.ReadLine();
            string[,]? filteredCars;

            switch (command)

            {
                case "1":                    
                    filterByMake(inventory, out filteredCars);
                     ShowVehicles(filteredCars);
                    break;
                case "2":
                    filterByPrice(inventory, out filteredCars);
                     ShowVehicles(filteredCars);
                    break;
                case "3":
                    filterByColor(inventory, out filteredCars);
                     ShowVehicles(filteredCars);
                    break;
                case "4":
                    ShowVehicles(inventory);
                    break;
/*                case "5":
                    AddVehicle(inventory);
                    break;*/
                default:
                    Console.WriteLine("Invalid Input");
                    break;
            }
        }

        static void filterByMake(string[,] inventory, out string[,] filterByMakeCars)
        {
            Console.WriteLine("Enter a make:");
            string make = Console.ReadLine();
            Console.WriteLine("Enter a model:");
            string model = Console.ReadLine();
            int i = 0;
            filterByMakeCars = new string[1, 6];

            for (int j = 0; j < inventory.GetLength(1); j++)
            {
                if (inventory[j, 1] == make || inventory[j, 2] == model) 
                {                    
                    for (int k = 0; k < inventory.GetLength(0); k++)
                    {
                        if (inventory[j,k] != null) {
                            filterByMakeCars[i, k] = inventory[j, k];
                        }                     
                    }
                    i++;
                }
            }

        }

        static void filterByPrice(string[,] inventory, out string[,] filterByPriceCars)
        {
            Console.WriteLine("Enter a min price:");
            decimal minPrice = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Enter a max price:");
            decimal maxPrice = Convert.ToDecimal(Console.ReadLine());
            int i = 0;
            filterByPriceCars = new string[6, 6];

            for (int j = 0; j < inventory.GetLength(1); j++)
            {
                decimal inventoryPrice = 0;
                if (inventory[j, 4] != null)
                {
                    inventoryPrice = Convert.ToDecimal(inventory[j, 4]);
                }

                if (minPrice <= inventoryPrice && inventoryPrice < maxPrice)
                {
                    for (int k = 0; k < inventory.GetLength(0); k++)
                    {
                        if (inventory[j, k] != null)
                        {
                            filterByPriceCars[i, k] = inventory[j, k];
                        }

                    }
                    i++;
                }
            }
            
        }

        static void filterByColor(string[,] inventory, out string[,] filterByColorCars)
        {
            Console.WriteLine("Enter a color:");
            string color = Console.ReadLine().ToUpper();
            int i = 0;
            filterByColorCars = new string[1, 6];

            for (int j = 0; j < inventory.GetLength(1); j++)
            {
                string inventoryColor = "";
                if (inventory[j, 3] != null) {
                    inventoryColor = inventory[j, 3].ToUpper();
                }
                
                if (inventoryColor == color)
                {

                    for (int k = 0; k < inventory.GetLength(0); k++)
                    {
                        if (inventory[j, k] != null)
                        {
                            filterByColorCars[i, k] = inventory[j, k];
                        }

                    }
                    i++;
                }
            }
        }

        static void ShowVehicles(string[,] inventory)
        {
            string command;
            int carSelection = 0;
            do
            {
                Console.WriteLine("Choose a vehicle to learn more: ");
                for (int i = 0; i < inventory.GetLength(0); i++)
                {
                    if (inventory[i, 0] != null) {
                        Console.WriteLine("{0} - {1} {2}", i + 1, inventory[i, 1], inventory[i, 2]);

                    }
                }
                carSelection = Convert.ToInt16(Console.ReadLine());
                string id = inventory[carSelection - 1, 0];
                string make = inventory[carSelection - 1, 1];
                string model = inventory[carSelection - 1, 2];
                string color = inventory[carSelection - 1, 3];
                string odometer = inventory[carSelection - 1, 4];
                string price = inventory[carSelection - 1, 5];

                Console.WriteLine("ID: {0}", id);
                Console.WriteLine("make: {0}", make);
                Console.WriteLine("model: {0}", model);
                Console.WriteLine("color: {0}", color);
                Console.WriteLine("odometer: {0}", odometer);
                Console.WriteLine("price: {0}", price);
                Console.WriteLine("---------------");
                Console.WriteLine("Choose an option: ");


                Console.WriteLine("P - Purchase Car");
                Console.WriteLine("V - Return to Choose a Vehicle Menu");
                Console.WriteLine("M - Return to Choose Main Menu"); 
                command = Console.ReadLine().ToUpper();
            } while (command == "V");
                if (command == "P") {
                    CreateInvoice(inventory, carSelection);
                    Console.WriteLine("Thank you for your purchase!");
                }
                if (command == "M")
                {
                    MainMenu(inventory);
                }
            }

        static void CreateInvoice(string[,] inventory, int carSelection)
        {
            string ownerName;
            string license;
            string payemntType;
            string address;
            string city;
            string state;
            string zip;
            string phone;
            string paymentType;
            string titleDate = DateTime.Now.ToString("MM - dd - yyyy");

            string id = inventory[carSelection - 1, 0];
            string make = inventory[carSelection - 1, 1];
            string model = inventory[carSelection - 1, 2];
            string color = inventory[carSelection - 1, 3];
            string odometer = inventory[carSelection - 1, 4];
            string price = inventory[carSelection - 1, 5];

            if (carSelection == 0)
            {
                Console.WriteLine("**Error: invalid selection");
            }
            else {
                Console.WriteLine("Enter Owner Name:");
                ownerName = Console.ReadLine();
                Console.WriteLine("Enter License Number:");
                license = Console.ReadLine();
                Console.WriteLine("Enter Address:");
                address = Console.ReadLine();
                Console.WriteLine("Enter City:");
                city = Console.ReadLine();
                Console.WriteLine("Enter State:");
                state = Console.ReadLine();
                Console.WriteLine("Enter Zip:");
                zip = Console.ReadLine();
                Console.WriteLine("Enter Phone:");
                phone = Console.ReadLine();
                Console.WriteLine("Payment Type:");
                paymentType = Console.ReadLine();

                string currentDt = DateTime.Now.ToString("yyyyMMddHHmmss");
                string invoiceName = "carInvoice" + ownerName + "_" + currentDt + ".txt";
                string fileName = @"C:\Academy\TestFiles\" + invoiceName;

                using (StreamWriter outputFile =
                new StreamWriter(fileName, true))
                {
                    outputFile.WriteLine(
                        "CAR INFORMATION"
                    );
                    outputFile.WriteLine(
                        "------------------"
                    );
                    outputFile.WriteLine(
                        "Car ID: {0}", id
                    );
                    outputFile.WriteLine(
                        "Car Make: {0}", make
                    );
                    outputFile.WriteLine(
                        "Car Model: {0}", model
                    );
                    outputFile.WriteLine(
                        "Car Color: {0}", color
                    );
                                        outputFile.WriteLine(
                        "Car Odometer: {0}", odometer
                    );
                                        outputFile.WriteLine(
                        "Car Price: {0:C}", price
                    );
                    outputFile.WriteLine(
                        "------------------"
                    );
                    outputFile.WriteLine(
                        "DRIVER INFORMATION"
                    );
                    outputFile.WriteLine(
                        "------------------"
                    );
                    outputFile.WriteLine(
                        "Owner Name: {0}", ownerName
                    );
                    outputFile.WriteLine(
                        "License #: {0}", license
                    );
                    outputFile.WriteLine(
                        "Address: {0}", address
                    );
                    outputFile.WriteLine(
                        "City: {0}", city
                    );
                    outputFile.WriteLine(
                        "Zip: {0}", zip
                    );
                    outputFile.WriteLine(
                        "Phone: {0}", phone
                    );
                    outputFile.WriteLine(
                        "Payment Type: {0}", paymentType
                    );
                    outputFile.WriteLine(
                        "------------------"
                    );
                }
            }
        }

            static void ReadFile(out string[,] inventory)
        {
            string name = "cars.txt";
            string fileName = @"C:\Academy\TestFiles\" + name;
            StreamReader inputFile = null;

            inventory = CreateArray();
            try
            {
                inputFile = new StreamReader(fileName);
                int i = 0;
               
                while (!inputFile.EndOfStream)
                {
                    string fileLine = inputFile.ReadLine();
                    string[] lineItems = fileLine.Split(',');

                        for (int j = 0; j < inventory.GetLength(1); j++)
                        {
                            inventory[i, j] = lineItems[j];
                        }
                        i++;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error opening file: {ex.Message}");
            }
            finally
            {
                if (inputFile != null) inputFile.Close();
            }         
        }

        static public string[,] CreateArray()
        {
            string[,] table = new string[6, 6];
            return table;
        }

    }
}